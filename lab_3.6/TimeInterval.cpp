#include "TimeInterval.h"

TimeInterval::TimeInterval()
{
	this->day = 0;
	this->hour = 0;
	this->minute = 0;
}

TimeInterval::TimeInterval(int day, int hour, int minute)
{
	DateTime::validateTime(abs(hour), abs(minute));

	this->day = day;
	this->hour = hour;
	this->minute = minute;
}

void TimeInterval::setDay(int day)
{
	this->day = day;
}

/// <summary>
/// ������ �����. ����� ��������� ������������� ��������
/// </summary>
/// <param name="hour">����</param>
/// <param name="minute">������</param>
void TimeInterval::setTime(int hour, int minute)
{
	DateTime::validateTime(abs(hour), abs(minute));

	this->hour = hour; this->minute = minute;
}

/// <summary>
/// ������� ������ �� �����
/// </summary>
void TimeInterval::print()
{
	std::cout <<
		day << " days, " <<
		hour << " hours, " <<
		minute << " minutes\n";
}